var csv = require("csvtojson");
const csvFilePath = "./primaryschool.csv";
const School=require("./db");
const mongoose=require("mongoose");

const converter = csv({
  includeColumns: [0, 1, 3, 4, 6, 7, 8]
})
  .fromFile(csvFilePath)
  .on("json", jsonObj => {
    School.set(jsonObj, (err, data) => {
      if (err) {
        throw err;
      } else {
        console.log(data);
      }
    });
  })
  .on("done", error => {
    console.log("end");
  });
