var mongoose = require("mongoose");
var schoolSchema = mongoose.Schema({
  id: {
    type: Number,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  cat: {
    type: String,
    required: true
  },
  moi: {
    type: String
    //required :true
  },
  cluster_name: {
    type: String,
    required: true
  },
  block_name: {
    type: String,
    required: true
  },
  district_name: {
    type: String,
    required: true
  }
});

var School = mongoose.model("School", schoolSchema);

module.exports.set = (data, callback) => {

  School.create(data, callback);
};
module.exports.dist = callback => {

  School.distinct("district_name", callback);
};
module.exports.getBlock = (dist, callback) => {
  let query="district_name."+dist;
   School.distinct("block_name",{district_name:dist}, callback);

};

module.exports.getCat = (dist, callback) => {
  School.aggregate([
  {$match:{block_name:dist}},
  {$group:{_id:"$cat",count:{$sum:1}}}
]).exec(callback)

};
