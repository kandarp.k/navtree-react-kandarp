import React, { Component } from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import "../css/style.css";
import Districts from './components/districts'

let data={};

class Node extends Component{
  constructor(){
    super();
    this.state={
      isToggled:false,
      dist:[]
    }
  }
  componentDidMount() {
    axios.get("http://localhost:2000/districts").then(res => {
      const dist = res.data;
      //data[dist]=dist;
      this.setState({dist});
      });
  }
  render(){
    /*let style
    if (!this.state.childVisible) {
      style = {display: 'none'}
    }*//*console.log(this.state.dist);
   */ return(this.state.dist.map((dist)=><Districts key={dist} districts={dist} />))


  }

}

ReactDOM.render(<Node />, document.getElementById("app"));
