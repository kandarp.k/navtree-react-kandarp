import React, { Component } from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import Blocks from './blocks';

class Districts extends Component{

    constructor(props){
      super(props);
      this.state={
        blocks:[],
        isClicked:false
      }
      this.getBlocks=this.getBlocks.bind(this);
      this.toggleClass=this.toggleClass.bind(this);
    }
    getBlocks(dist){
      axios.get("http://localhost:2000/blocks/"+dist).then(res => {
            const blocks = res.data;
            this.setState({blocks})

            });
    }
    toggleClass(){
      if(!this.state.isClicked){
        return "hide second"
      }
      else{
        return "second"
      }
    }
    render(){
      console.log(this.props.districts);
        return(
          <div className="dist">
          <li className="first" onClick={()=>{this.setState({isClicked:!this.state.isClicked});this.getBlocks(this.props.districts)}}>
            {this.props.districts}
          </li>
          <ul className={this.toggleClass()}>
            {this.state.blocks.map((block)=><Blocks key={block} blocks={block}/>)}
          </ul>
          </div>
        )
    }
}

export default Districts;
