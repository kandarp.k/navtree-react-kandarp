import React, { Component } from "react";
import ReactDOM from "react-dom";
import axios from "axios";

class Blocks extends Component{
  constructor(props){
    super(props);
    this.state={
      isClicked:false,
      cat:[]
    }
  }
  getCat(bl){
    axios.get("http://localhost:2000/categories/"+bl).then(res=>{
      this.setState({cat:res.data})
      console.log(this.state.cat);
    })
  }
  toggleClass(){
    if(!this.state.isClicked){
      return "hide second";
    }
    else {
      return "second";
    }
  }

  render(){
    return (<div className="categories">
          <li className="first" onClick={()=>{this.setState({isClicked:!this.state.isClicked});this.getCat(this.props.blocks) }}>
          {this.props.blocks}
          </li>
          <ul className={this.toggleClass()}>
          {this.state.cat.map(categ=><li key={categ["_id"]}>{categ["_id"]}:{categ["count"]}</li>)}
          </ul>
        </div>)
  }
}

export default Blocks;
