var School = require("./db1");
var mongoose = require("mongoose");
var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var cors = require("cors");

app.use(
  cors({
    methods: ["GET", "POST"]
  })
);
app.use(bodyParser.json());
mongoose.connect("mongodb://localhost/primaryschool");

var getCat = (res, dist) => {
  School.getCat(dist, (err, data) => {
    if (err) {
      throw err;
    } else {
      if(data.length>0){
        console.log(data);
      return res.json(data);}
      else{
        res.send("404");
      }
    }
  });
};

var getBlock = (res, dist) => {
  School.getBlock(dist, (err, data) => {
    if (err) {
      throw err;
    } else {
      if(data.length>0){

      return res.json(data);}
      else{
        res.send("404");
      }
      //console.log(data);
    }
  });
};
var getDistrict = res => {
  School.dist((err, data) => {
    if (err) {
      throw err;
    } else {
      return res.json(data);
    }
  });
};

app.get("/", (req, res) => {
  res.send(
    "1.Go to /district for District List.\n 2Go to /block for Block List.\n 3.Go to /category for category List."
  );
});
app.get("/districts", (req, res) => {
  getDistrict(res);

});
app.get("/categories/:dist", (req, res) => {
  console.log(req.params.dist);
  getCat(res, req.params.dist);
});
app.get("/blocks/:dist", (req, res) => {
  getBlock(res, req.params.dist);
});
app.listen(2000);
